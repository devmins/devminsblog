<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
    <title>Devmins</title>

    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>

.fa {
  padding: 6px ;
  font-size: 36px;
  width: 48px;
  height: 50px
  text-align: center !important;
  text-decoration: none;
  margin: 5px 2px;
  border-radius: 50%;
}

.fa:hover {
    opacity: 0.7;
}

.fa-facebook {
  background: #3B5998;
  color: white;
}

.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-google {
  background: #dd4b39;
  color: white;
}

.fa-linkedin {
  background: #007bb5;
  color: white;
}

.fa-instagram {
  background: #125688;
  color: white;
}

</style>
  </head>
  <body>

    <!--Main Navbar-->
    <nav class="navbar navbar-expand-md  navbar-dark fixed-top" style="padding-bottom:10px;padding-top:10px;background-color:	#505050 ;" >
        <div class="container">
            <a class="navbar-brand" href="index.php">DevMins</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
              <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="index.php"><b>Home</b></a>
                  </li>

                 <li class="nav-item dropdown">
                   <a class="nav-link" href="explore.php"><b>Explore</b></a>
                 </li>

                 <li class="nav-item">
                   <a class="nav-link" href="contact.php"><b>Inbox</b></a>
                 </li>

                <li class="nav-item">
                  <a class="nav-link" href="post.php"><b>Messaging</b></a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#"><b>Activity</b></a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#"><b>Account</b></a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#"><b>Post</b></a>
                </li>
              </ul>
            </div>
        </div>
    </nav>
    <!-- Main Navbar-->


<br>
<br>
<br>
<br>


<!-- post Categories-->
<div class="container-fluid ">
 <ul class="nav justify-content-center">
   <li class="nav-item">
     <a class="nav-link" href="explore.php"><b>FOOD</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="coding.php"><b>CODING</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="tech.php"><b>TECH</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="lifestyle.php"><b>LIFESTYLE</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="design.php"><b>DESIGN</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="science.php"><b>SCIENCE</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="media.php"><b>MEDIA</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="politics.php"><b>POLITICS</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="self.php"><b>SELF</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="more.php"><b>MORE</b></a>
   </li>
 </ul>
</div>
   <!--post Categories-->


<br>
<br>
<br>

    <!--Post title-->
      <div class="container" style="margin-left:190px;margin-top:50px";>
        <h1>SCIENCE</h1>
        <h6>Science is a beatifull gift to humanity.</h6>
        <br>
      </div>
    <!--Post title-->


                                  <!--Posts-->
<!--Row1-->
<div class="container" style="padding-left:100px;">
  <hr>
  <!--1-->
    <div class="card-columns" style="margin-left:10px;">
       <div class="card" style="width:320px";>
         <div class="card-header" style="background:white !important;">
           <div class="row">
             <div class="col-md-2">
               <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
             </div>
             <div class="col-md-5">
               <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
               <span style="font-size:10px;margin-left:4px !important">harison96</span>
             </div>
             <div class="col-md-5">
               <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
             </div>
           </div>
         </div>
             <img class="card-img-left" src="img/tech.jpg" alt="image Can't load" width="100%" height="200px">
             <div class="card-body">
          <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
          <a data-toggle="modal" data-target="#exampleDeck" class="text-primary">Read More</a>
      </div>
      <!-- Modal -->
<div class="modal fade" id="exampleDeck" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
  <div class="modal-header">
    <h5 class="modal-title text-success text-center"  id="exampleDeckLabel" >Minimalist Baker</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
<blockquote>
 You can love me or hate me for this, but this recipe is also meant to be however you want it to be.
You can add or subtract those filler ingredients like no one’s business – omit corn, add beans, swap
more vegetables for chicken. Whatever you want, girl. Whatever. You. Want.
Although it’s not really what people think of as Instant Pot season, I find that the IP treats me well year-round. Lately my dinner life looks like this:
</blockquote>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
  </div>
</div>
</div>
</div>
</div>

<!--2-->
    <div class="card" style="width:320px";>
      <div class="card-header" style="background:white !important;">
        <div class="row">
          <div class="col-md-2">
            <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
          </div>
          <div class="col-md-5">
            <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
            <span style="font-size:10px;margin-left:4px !important">harison96</span>
          </div>
          <div class="col-md-5">
            <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
          </div>
        </div>
      </div>
          <img class="card-img-left" src="img/tech1.jpg" alt="image Can't load" width="100%" height="200px">
          <div class="card-body">
       <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
    <a data-toggle="modal" data-target="#exampleDeck-a" class="text-primary">Read More</a>
   </div>
   <!-- Modal -->
<div class="modal fade" id="exampleDeck-a" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-a" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
 <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-a" >Minimalist Baker</h5>
 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
   <span aria-hidden="true">&times;</span>
 </button>
</div>
<div class="modal-body">
<blockquote>
Spring = warmer weather = eating less soup! That being said… This spring soup = eating more soup.
It’s that perfect blend of comforting but also fresh-feeling that leaves you a) wanting to lick the bowl,
 and b) feeling really good about yourself when it’s later in the day and you remember back to all the
 veggies you ate for lunch. It’s LOADED TO THE MAX with veggies and whole grains and protein, and also big, yummy, Italian-style flavor.
It’s also kind of a blank canvas for toppings, which, as you know, are my weakness – lemon juice? Parmesan?
fresh basil and parsley? salt and pepper? whole milk Greek yogurt? I can’t say I don’t like it all.
</blockquote>
</div>
<div class="modal-footer">
 <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
    </div>
 </div>

<!--3-->
 <div class="card" style="width:320px";>
   <div class="card-header" style="background:white !important;">
     <div class="row">
       <div class="col-md-2">
         <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
       </div>
       <div class="col-md-5">
         <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
         <span style="font-size:10px;margin-left:4px !important">harison96</span>
       </div>
       <div class="col-md-5">
         <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
       </div>
     </div>
   </div>
       <img class="card-img-left" src="img/tech2.jpg" alt="image Can't load" width="100%" height="200px">
       <div class="card-body">
    <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
  <a data-toggle="modal" data-target="#exampleDeck-b" class="text-primary">Read More</a>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleDeck-b" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-b" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-b" >Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
Bjork and I are so excited and nervous and just everything about this tiny bloom.
She’s a she (!!), and she’s 18 weeks, and we’re taking it one day at a time while
 staying cautiously hopeful that we will get to bring her home with us this fall.
To be honest, most days it’s hard to know exactly how to talk about this. Every
sentence out of my mouth starts with “if” instead of “when.” Most women feel more
 safe the further along they are in a pregnancy, but I am acutely aware that there
 really is no safe zone ever. And that’s really not a good thing to know.
The one thing I do know for sure how to say is thank you so much for all your messages
of love and support over the last year through the loss of our baby boy Afton. We will
miss him forever, and you’ve carried that love so beautifully with us.
Even though it’s “just a food blog,” I have to say, you are all the best people on the
internet. I can just feel it through the screen.So here’s to new lov
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
  </div>
</div>


 <!--Row 2-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--4-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-left" src="img/tech3.jpg" alt="image Can't load" width="100%" height="200px">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
            <a data-toggle="modal" data-target="#exampleDeck-c" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
 <div class="modal fade" id="exampleDeck-c" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-c" aria-hidden="true">
 <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-c" >Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
 <blockquote>
   You can love me or hate me for this, but this recipe is also meant to be however you want it to be.
 You can add or subtract those filler ingredients like no one’s business – omit corn, add beans, swap
 more vegetables for chicken. Whatever you want, girl. Whatever. You. Want.
 Although it’s not really what people think of as Instant Pot season, I find that the IP treats me well year-round. Lately my dinner life looks like this:
 Come home.
 Chop vegetables, measure spices, check the recipe one more time.
 Start Instant Pot.
 Go sit outside and look at a magazine in the sun. ☀
 Come back in and eat.
 I mean, dang if that isn’t a really good way to make dinner.
 (And guys, if you don’t have an Instant Pot, that’s okay, too – I will put some alternative directions for you in the recipe notes section!
 </blockquote>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
  </div>
 </div>
 </div>
</div>

  <!--5-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-left" src="img/tech4.jpg" alt="image Can't load" width="100%" height="200px">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
      <a data-toggle="modal" data-target="#exampleDeck-d" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
<div class="modal fade" id="exampleDeck-d" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-d" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
 <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-d" >Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
 </div>
 <div class="modal-body">
<blockquote>
  Spring = warmer weather = eating less soup! That being said… This spring soup = eating more soup.
  It’s that perfect blend of comforting but also fresh-feeling that leaves you a) wanting to lick the bowl,
   and b) feeling really good about yourself when it’s later in the day and you remember back to all the
   veggies you ate for lunch. It’s LOADED TO THE MAX with veggies and whole grains and protein, and also big, yummy, Italian-style flavor.
  It’s also kind of a blank canvas for toppings, which, as you know, are my weakness – lemon juice? Parmesan?
  fresh basil and parsley? salt and pepper? whole milk Greek yogurt? I can’t say I don’t like it all.
</blockquote>
 </div>
 <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
 </div>
</div>
</div>
</div>
   </div>

<!--6-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-left" src="img/tech5.jpg" alt="image Can't load" width="100%" height="200px">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
    <a data-toggle="modal" data-target="#exampleDeck-e" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-e" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-e" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-e" >Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
  Bjork and I are so excited and nervous and just everything about this tiny bloom.
  She’s a she (!!), and she’s 18 weeks, and we’re taking it one day at a time while
   staying cautiously hopeful that we will get to bring her home with us this fall.
  To be honest, most days it’s hard to know exactly how to talk about this. Every
  sentence out of my mouth starts with “if” instead of “when.” Most women feel more
   safe the further along they are in a pregnancy, but I am acutely aware that there
   really is no safe zone ever. And that’s really not a good thing to know.
  The one thing I do know for sure how to say is thank you so much for all your messages
  of love and support over the last year through the loss of our baby boy Afton. We will
  miss him forever, and you’ve carried that love so beautifully with us.
  Even though it’s “just a food blog,” I have to say, you are all the best people on the
  internet. I can just feel it through the screen.So here’s to new lov
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
   </div>
</div>


<!--Row3-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--7-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-left" src="img/tech6.jpg" alt="image Can't load" width="100%" height="200px">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
            <a data-toggle="modal" data-target="#exampleDeck-f" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
 <div class="modal fade" id="exampleDeck-f" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-f" aria-hidden="true">
 <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-f" >Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
 <blockquote>
Bjork and I are so excited and nervous and just everything about this tiny bloom.
She’s a she (!!), and she’s 18 weeks, and we’re taking it one day at a time while
 staying cautiously hopeful that we will get to bring her home with us this fall.
To be honest, most days it’s hard to know exactly how to talk about this. Every
 sentence out of my mouth starts with “if” instead of “when.” Most women feel more
safe the further along they are in a pregnancy, but I am acutely aware that there
really is no safe zone ever. And that’s really not a good thing to know.
The one thing I do know for sure how to say is thank you so much for all your messages
of love and support over the last year through the loss of our baby boy Afton. We will
miss him forever, and you’ve carried that love so beautifully with us.
Even though it’s “just a food blog,” I have to say, you are all the best people on the internet. I can just feel it through the screen.
So here’s to new lov
 </blockquote>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
</div>
</div>
</div>
  </div>

  <!--8-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-left" src="img/tech7.jpg" alt="image Can't load" width="100%" height="200px">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
        <a data-toggle="modal" data-target="#exampleDeck-g" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
<div class="modal fade" id="exampleDeck-g" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-g" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
 <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-f" >Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
 </div>
 <div class="modal-body">
<blockquote>
  Okay, here’s what I’ve put this on so far.
  summer cobb salad (coming soon!)
  grilled chipotle chicken, as in DIPPED IT STRAIGHT IN LIKE A MONSTER
  a mashed avocado – instaguac, anyone?
  breakfast tacos
  chorizo tacos
  chicken tacos
  roasted potatoes
  grilled sweet corn
  rice to make “green rice” for meal prep rice bowls
  a cheese quesadilla
  another cheese quesadilla
  one more cheese quesadilla, because I’m eating a lot of quesadillas lately, okay? just let me.
  It is such a stupidly easy recipe, but I didn’t want it to get lost. I felt it needed its own post.
  It’s not fancy. It will take you two seconds.
  But it will elevate your summer eating game to places you definitely want to be.
</blockquote>
 </div>
 <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
 </div>
</div>
</div>
</div>
   </div>


<!--9-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-left" src="img/tech8.jpg" alt="image Can't load" width="100%" height="200px">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
  <a data-toggle="modal" data-target="#exampleDeck-h" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-h" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-h" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-h" >Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
  he more “involved” part of the recipe (if you can even call it that) is just plopping your chicken in some spices and a
  few chipotle peppers and letting it marinate for a while before grilled.WORTH IT, you guys! So worth it.
  Also probably more normal to take a picture of the grilled chicken rather than the raw marinated stuff,
  but who said anything about normal.Bjork and I ate this salad on the deck on a random Tuesday night and we both mega-loved every lip-lickin juicy bite.
It’s a little spicy, a lot crunchy, and those bites of sweetness in between are so super rewarding.
Alright, you guys. It’s time to back it up just a little bit.
There is a really good summery salad coming your way, but for
 today, we need to strip it down to the basics. Today it’s all about that dressing.
My requirements for a new dressing in my life is that it doesn’t take more than a hot minute to make it and
tastes so good that I actually WANT to eat salad every minute of every day. This does both! with just a handful of very regular ingredients:
cilantro garlic vinegar olive oil salt Also, a pinch of red pepper flakes and a splash of water.
But for the most part, this is a five-ingredient, straight-out-the-blender, beautiful green miracle.
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
  </div>
</div>


<!--Row 4-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--10-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-left" src="img/tech9.jpg" alt="image Can't load" width="100%" height="200px">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
          <a data-toggle="modal" data-target="#exampleDeck-i" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
 <div class="modal fade" id="exampleDeck-i" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-i" aria-hidden="true">
 <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-i" >Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
 <blockquote>
   The more “involved” part of the recipe (if you can even call it that) is just
   plopping your chicken in some spices and a few chipotle peppers and letting it
   marinate for a while before grilled. WORTH IT, you guys! So worth it.
   Also probably more normal to take a picture of the grilled chicken rather than
    the raw marinated stuff, but who said anything about norma
 </blockquote>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
  </div>
 </div>
 </div>
</div>

  <!--11-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-left" src="img/tech10.jpg" alt="image Can't load" width="100%" height="200px">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
        <a data-toggle="modal" data-target="#exampleDeck-j" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
<div class="modal fade" id="exampleDeck-j" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-j" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
 <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-j" >Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
 </div>
 <div class="modal-body">
<blockquote>
  Salad is coming to our hearts today from the What’s Gaby Cooking Cookbook.
  Remember when I published those BEST EVER chicken tinga tacos and told you
  about my friend Melissa’s amazing book and then talked about how I hardly
  ever promote books? But then I turned around and told you how great THIS
  one was, too? LOL to me. It’s just dumb luck that both of these came out when they did.
  We’re not paid for any cookbook promotions – I just like to show you the ones that we
  like and give you a sneak at their best recipes.
  And this one is one that I like. And this might be one of its best recipes.
  Tied with the cilantro vinaigrette, which I couldn’t not use again for this salad.
  I know Gaby says to use the champagne vinaigrette with this salad, but GABY COME ON
   YOU CAN’T TEASE US LIKE THAT. One good lick of that cilantro dressing and you’re
   done. Goodbye. This is your life now
</blockquote>
 </div>
 <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
 </div>
</div>
</div>
</div>
   </div>

<!--12-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-left" src="img/tech11.jpg" alt="image Can't load" width="100%" height="200px">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
      <a data-toggle="modal" data-target="#exampleDeck-k" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-k" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-k" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-k" >Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
  You’re basically done.
  At this point, you grab your siggi’s of choice – again, preferably triple cream
   vanilla because IT TASTES LIKE ACTUAL CREAM and I swear it is the unicorn of all
   yogurts, but any siggi’s will do because they are all made with simple ingredients
   and – this is key – not a lot of sugar. Did I mention that already?
  You put some of that yummy, caramelized shortcakey yogurt into a bowl and swirl your
  creamy yogurt on top and crown the whole thing with a generous heap of juicy fresh strawberries.
  Boom. Strawberry Shortcake Yogurt Bowl.
  You are set. Summer snackage at its finest.
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
  </div>
</div>


<!--Row 5-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--13-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-left" src="img/tech12.jpg" alt="image Can't load" width="100%" height="200px">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
        <a data-toggle="modal" data-target="#exampleDeck-l" class="text-primary">Read More</a>
        </div>
        <!--Modal -->
 <div class="modal fade" id="exampleDeck-l" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-l" aria-hidden="true">
 <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-l" >Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
 <blockquote>
   This is the good stuff, so let’s start there.
This yogurt is creamy and rich like none other! It’s a perfect snack because it kind of tastes like ice cream except without the icky-feeling sugar bomb. It’s naturally high in protein, made with simple ingredients, and lower in sugar, which I really, really love because I am eating a lot of it. If you should find yourself wanting a lower fat version, there are so many options to choose from that are equally as lower-sugar and wonderful. But if you like to go creamy-dream-team on your snacks (AND I DO), the triple cream yogurt is where you need to be.
Without a doubt, siggi’s makes a great standalone snack in the summer – cold, creamy, nutritionally feel-good.
But sometimes, the Creamy likes to invite the Crunch to the party. Know what I mean?
So here’s the deal – the only real work in this “recipe” aka winning snack concept is making the granola. And even that part is less about work and more about just giving yourself a gift in the form of a big huge jar of fresh, toasty granola with all those crunchy caramelized bits that will make your house smell amazing.
 </blockquote>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
  </div>
 </div>
 </div>
</div>

  <!--14-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-left" src="img/tech13.jpg" alt="image Can't load" width="100%" height="200px">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
      <a data-toggle="modal" data-target="#exampleDeck-m" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
<div class="modal fade" id="exampleDeck-m" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-m" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
 <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-m" >Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
 </div>
 <div class="modal-body">
<blockquote>
  Pop quiz: what is…
  high in protein,
  lower in sugar,
  creamy on a whole other level,
  desserty, yet snack-able in all the right ways,
  and nutritious enough to eat for breakfast?
  Answer: Strawberry Shortcake Yogurt Bowls!
  Yeah, I just kind of made that concept up, but if you think about it, it totally works – we have the juicy strawberries, we have the cookie-esque shortcake flavor in more nutritious granola form, and we have a creaminess that rivals the most luscious whipped cream of all time thanks to the beauty of siggi’s triple cream vanilla yogurt.
  Hello, summer treat! Hello, breakfast all week! Hello, siggi’s Icelandic Skyr for life!
  In real life, it is very easy for me to talk about my love for siggi’s because I have been hard-core craving dairy these last few months (something about those first trimester calcium needs? I don’t know, just yum). The beauty of those yogurt cravings is that it has led to the opportunity for mass amounts of yogurt consumption. And truly, siggi’s is top notch.
</blockquote>
 </div>
 <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
 </div>
</div>
</div>
</div>
</div>

   <!--15-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-left" src="img/tech14.jpg" alt="image Can't load" width="100%" height="200px">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
    <a data-toggle="modal" data-target="#exampleDeck-n" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-n" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-n" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-n" >Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
  Short Rib Ragu?! Who am I right now, making SHORT RIBS?
  I am a pillowy-gnocchi-loving-pasta-eater, that’s who.
  Carbs 100% dictate my life, and my current carbs wanted short rib ragu.
  Did you know that short rib ragu is so good, end of story, but also not end of story because it just takes pasta to a whole nother level? Saucy, tangy, and just so tender and luscious all at the same time? This was a new discovery for me.
  As a less-savvy meat cooker d how we had this at our dinner club all those years ago, and I remembered how good it was with our tiny pillows of homemade gnocchi, and then I got brave with it because a) Instant Pot, and b) food motivation.
  So that’s what’s happening today – short ribs, short cut, Instant Pot, RAGU!
  Starting with the short ribs. We’re starting with the saute function on the Instant Pot to get a nice browning on the short ribs.
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
  </div>
</div>

<!--Row 6-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--16-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-top" src="img/tech15.jpg" alt="image Can't load" width="100%" height="200px">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
            <a data-toggle="modal" data-target="#exampleDeck-o" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
 <div class="modal fade" id="exampleDeck-o" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-o" aria-hidden="true">
 <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-o">Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
 <blockquote>
   You guys, this is going to be epic. Are your gnocchi minds even ready for this bonanza?
 Today we are talking HOW TO MAKE HOMEMADE GNOCCHI… party style. Because fresh homemade Italian food is best when made with friends and wine and excessive laughter.
 True story: in 2015, I went on a trip with DeLallo and had the chance to get up close and personal with the process of making homemade gnocchi. It was so glorious. There was a creamy mushroom sauce. There was wine and laughter. There was lots of cheese. I have never been the same.
 In 2016, I decided it was time to get more gnocchi in my life. So I rounded up my dinner club, and naturally they all showed up with homemade sauces, condiments, cheeses, and a willingness to roll those little bits of potato dough with me.
 AND IT WAS SO FUN. To this day, it is one of our most talked-about dinner club experiences, probably because a) hands-on is exciting, and b) the gnocchi was truly like little baby pillows of heavenly bliss. I don’t think we’ll ever not talk about it as one of our best-ever dinner club nights (our entire spread with multiple sauces, salad, and homemade bread is pictured at the beginning of this post).
 So make gnocchi, and have a party while doing it. That’s just what you gotta do. Are you ready?
 </blockquote>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
  </div>
 </div>
 </div>
</div>

<!--17-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-top" src="img/tech16.jpg" alt="image Can't load" width="100%" height="200px">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
        <a data-toggle="modal" data-target="#exampleDeck-p" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
<div class="modal fade" id="exampleDeck-p" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-p" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
 <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-p" >Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
 </div>
 <div class="modal-body">
<blockquote>
  So when I say anytime, I really do mean anytime.
  That’s what I love about these baked chicken meatballs. They are SO EASY to make and they can sort of turn into anything. Here are some of their best features:
  They are BAKED.
  They are CHICKEN. (Turkey’s good, too.)
  They use one bowl.
  There are no cutting boards involved.
  There is no splattery sautéing.
  They are meal-prep friendly.
  Also freezer friendly.
  THEY GO WITH ANYTHING, anytime.
  I’m not a meatball lover, but I LOVE THESE MEATBALLS.
  These baked chicken meatball bites can go with spaghetti and meatballs, baked rigatoni, or any pasta for that matter. They can be served with mashed cauliflower and green beans (my current favorite), or saved for quick and healthy lunch meal prep with roasted veggies. Half of the batch can be tossed with buffalo sauce and half can be frozen for next week’s Swedish meatball dinner.
  I don’t memorize many recipes – I have the magic green sauce, soft chocolate chip cookies, and wild rice soup burned into my brain, but that’s about it. THAT BEING SAID, this baked chicken meatball recipe is absolutely
</blockquote>
 </div>
 <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
 </div>
</div>
</div>
</div>
   </div>

<!--18-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-top" src="img/tech17.jpg" alt="image Can't load" width="100%" height="200px">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
    <a data-toggle="modal" data-target="#exampleDeck-q" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-q" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-q" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-q" >Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
  You know how sometimes Facebook feels a little blah?
  This month we decided to revive our Facebook VIP Group in an effort to make Facebook fun again. Look at all these fun people who are a part of the group! They are awesome. And fun. Did I mention also: fun?
  Want to look at pictures of everyone’s pets and see what kitchen tools people love and talk about what you’re making for dinner? This group is your place. It’s kind of like the coffee date, but more often.
  The group is moderated by both me and Abby, our social media queen, and it’s been a blast. You should join us!
  As usual, Sage would like to close out the coffee date with an inspirational message
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<!--Row 7-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--19-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-top" src="img/tech18.jpg" alt="image Can't load" width="100%" height="200px">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
            <a data-toggle="modal" data-target="#exampleDeck-r" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
 <div class="modal fade" id="exampleDeck-r" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-r" aria-hidden="true">
 <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-r" >Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
 <blockquote>
   Maybe I’m trying to make up for lost time from the first trimester when the only thing I cooked or ate was deliciously cheap packets of noodle soup, or maybe I’m finally becoming an adult.
   Either way, this month I’ve been a lot more intentional about meal prep and IT’S BEEN SO AWESOME! Less waste, more intentional grocery shopping, and healthier eating.
   Here’s my system, if you can call it that.
   On Sunday, I do these three things:
   I prep overnight oats for breakfast to last for the week.
   I prep one single lunch recipe to last for the week. Usually this is something simple like chicken and rice bowls, roasted veggies and tofu, summer soup, etc.
   I look at the calendar to see how many times we’ll be eating dinner at home that week. Guess what? Usually 2-3 dinner recipes during the week is more than enough for the two of us. Once I pick the 2-3 recipes for the week, I order my groceries to be delivered via Instacart. And then I write our weekly meals into my bullet journal so I actually remember that I do, in fact, have a plan when I start to feel the pull towards frozen pizza.
   With this (very loose) system, we are eating healthier, and by the end of the week, the fridge is clean and empty – just how I like it. We can eat out guilt-free during the weekend (brunch! brunch! brunch!) and re-start the system again on Sunday.
   Boom! Look at me go, mom! I’m a real meal planner!
 </blockquote>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
  </div>
 </div>
 </div>
      </div>

<!--20-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-top" src="img/tech19.jpg" alt="image Can't load" width="100%" height="200px">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
         <a data-toggle="modal" data-target="#exampleDeck-s" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
<div class="modal fade" id="exampleDeck-s" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-s" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
 <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-s" >Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
 </div>
 <div class="modal-body">
<blockquote>
  Do you guys wash your hair every day? People say it’s better for your hair – I say it’s easier for my life.
  Enter: a shower cap. A real live shower cap. This is the one I bought.
  I’m not going to say it’s cute. I know they try to sell it as cute and fashionable, but please. Come on. This really does not look cute on anyone, does it? Even floral patterns can’t make you NOT look like an instant mushroom-head.
  But on the plus side, it’s awesome at keeping your hair fresh, smooth, and dry while still showering in between hair washes. It’s made from a really nice sturdy-but-soft-ish material and has a tight (but manageable) seal around the outside to prevent those little frizzies around your face. I love it. I use it on ALL Day Two and Day Three hair days.
  Do you wash your hair every day? Every other? Yeah, we’re getting personal here.
</blockquote>
 </div>
 <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
 </div>
</div>
</div>
</div>
   </div>

<!--21-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-top" src="img/tech20.jpg" alt="image Can't load" width="100%" height="200px">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
    <a data-toggle="modal" data-target="#exampleDeck-t" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-t" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-t" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-t" >Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
  (It’s not like my closet was exactly that clean, okay? I definitely did move an overflowing laundry basket and a few random straggler socks out of the way for this picture.)
  Remember how I was super into the capsule wardrobe thing? It’s still going strong. Here are the ideals that keep me devoted to the capsule concept:
  Buy things you 100% love
  Buy things that 100% fit
  Buy things that 100% fit your lifestyle
  Buy less
  However: one small challenge to the capsule = being pregnant again.
  With what my doctor nicely described as above average weight gain (shoutout to banana cream pies, raspberry crumble bars, and mocha peanut butter pie which will be coming to the blog soon) AND a larger-than-average 21 week baby in there, it’s safe to say that I have now fully transitioned to a completely maternity wardrobe. I know a lot of people hate maternity clothes, but I actually don’t mind them – a) comfortable, and b) special. I really longed to wear these clothes last year.
  So the fact that they are maternity clothes is not a problem, but the extra few all-around pounds plus the limited choices in styles has made the categories of love and fit a tiiiny bit challenging.
  So far, here’s what’s in my summer capsule:
  a few maternity layering tanks / tees from Target
  a few pairs of maternity shorts from A Pea in the Pod
  a few maternity tops from A Pea in the Pod and Motherhood Maternity (and I’m sorry but I really dislike that name, MM)
  It’s a good start, but I still feel like I’m a little lacking. For example, I love to wear summer dresses, and I have yet to find a maternity dress that fits somewhere between the styles of draped tent and rubber band.
  So, in terms of cute tops, dresses, and other summer maternity essentials… what are your suggestions for me? Where to shop, what to buy? Keep in mind I work from home and never wear anything remotely fancy and dog hair is a permanent fixture of my lifestyle.
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
   </div>
</div>



<!--Row8-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--22-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-top" src="img/tech21.jpg" alt="image Can't load" width="100%" height="200px">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
          <a data-toggle="modal" data-target="#exampleDeck-u" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
 <div class="modal fade" id="exampleDeck-u" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-u" aria-hidden="true">
 <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-u" >Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
 <blockquote>
   YEAH WE WENT THERE. Like, literally went there.
American Ninja Warrior recorded part of this next season here in Minneapolis, and Bjork’s actual-ninja-cousin got to compete, and we went and watched it live, and it was probably the highlight of my summer.
I can’t really say much about what happened and YOU KNOW THIS IS HARD FOR ME but I would just like you to keep an eye out for Leif, The Swedish Ninja, and his two top-nerd-level fans (hint: both wearing glasses) in the stands during the Minneapolis qualifiers on this season of American Ninja Warrior.
Any ANW fans or – better yet – ANW athletes out there?
 </blockquote>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
  </div>
 </div>
 </div>
      </div>

<!--23-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-top" src="img/tech22.jpg" alt="image Can't load" width="100%" height="200px">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
         <a data-toggle="modal" data-target="#exampleDeck-v" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
<div class="modal fade" id="exampleDeck-v" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-v" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
 <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-v" >Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
 </div>
 <div class="modal-body">
<blockquote>
  If we were having real coffee together, we’d be sitting outside on my deck, and I’d tell you to look over the edge of the deck so you could see the little blooms on my Lenten rose, and we’d talk about how it is so amazing that this unlikely plant was able to be transferred from a small pot in my house, into the soil in the ground, and covered with snow for nearly 6 months, and un-expertly over-trimmed this spring by yours truly, and STILL eventually sprout these beautiful little roses again.
  Nature is one big miracle.
  But first, what are you drinking today? For me, it’s the same green smoothie as every other day: mango, spinach, water, and Bolthouse Green Goodness juice (because it’s a little creamier and makes the texture of the smoothie basically like an ice cream shake), but depending on the moment could also be sparkling water and coffee and homemade strawberry lemonade if we’re past 6pm.
  Grab your tea, your iced coffee, your sparkling water… time for our monthly coffee date.
</blockquote>
 </div>
 <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
 </div>
</div>
</div>
</div>
   </div>

<!--24-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-top" src="img/tech23.jpg" alt="image Can't load" width="100%" height="200px">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
    <a data-toggle="modal" data-target="#exampleDeck-w" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-w" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-w" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-w" >Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
Let’s just start there, okay?
Because THERE is a very good place to start. It involves:
Quinoa
Cucumber
Tomato
Sun dried peppers
Feta cheese
Grilled shrimp
Curlycue greens
And a bucket of lemon dill dressing.
That would be enough, wouldn’t it? (I just ate it for lunch and I can assure you that it is and was more than enough.)
BUT.
I like it when things are a little more fun, a little more convenient, a little more real-life-able. Which is why this post is actually called Summer Salad Jars.
As in, let’s put this salad right on up in a jaDo you know what’s beautiful about this? The jar thing?
A) Jars. They look pretty in your fridge, and they SUPER CONVENIENT. It’s lunch in literally two shakes: shake the jar out, and toss it all up.
B) The tomatoes and the dressing sit in the bottom of the jar together and actually get BETTER and JUICIER and MORE FLAVOR LOADED the longer they sit in the fridge. The quinoa, too – it can handle that little bit of moisture from the dressing! Think of that lemony, garlicky, dill-punch of flavor just hanging out in there, giving your quinoa the royal treatment. How about a YES MA’AM on that one.
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
   </div>
 </div>


<!--Row 9-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--25-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-top" src="img/tech24.jpg" alt="image Can't load" width="100%" height="200px">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
            <a data-toggle="modal" data-target="#exampleDeck-x" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
 <div class="modal fade" id="exampleDeck-x" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-x" aria-hidden="true">
 <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-x" >Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
 <blockquote>
   And yes, you will want to eat it. I know you’re not supposed to eat raw flour and all that, but really? Not even a few little crumble chunks? I mean, good luck with that.
   On to the raspberry filling. This is also eat-with-a-spoon-able delicious. I like this real raspberry filling better than a jam filling layer because the jam ones just get so sweet sometimes, know what I mean?
   We’re talking about primarily:
   Raspberries
   Lemon juice
   Sugar
 </blockquote>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
  </div>
 </div>
 </div>
      </div>

<!--26-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-top" src="img/tech25.jpg" alt="image Can't load" width="100%" height="200px">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
      <a data-toggle="modal" data-target="#exampleDeck-y" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
<div class="modal fade" id="exampleDeck-y" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-y" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
 <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-y" >Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
 </div>
 <div class="modal-body">
<blockquote>
  Sweet, soft, wonderfully juicy raspberry crumble bars.
  With a soft oat-brown-sugar-butter cookie-like base, and a thick, sweet, and tart but not overwhelming layer of raspberries in the middle, tucked in by a light dusting of those subtle streusel-like crumbs on top.
  You’re gonna need to be making these.
  I have strong opinions about fruit bars. I like them to be soft, not crunchy. I like them to be fruity, but not overloaded with fruit. I like them to taste kind of like cookies, but not a crunchy cookie and definitely not anything resembling the flavor of almond extract. I just… that’s not for me. I’ve eaten enough bars in church basements in my life to know what I like.
  And what I like is THIS ONE.
  The crust is soft and delicious and mostly made from our four usual suspects:
  oats
  flour
  brown sugar
  butter
</blockquote>
 </div>
 <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
 </div>
</div>
</div>
</div>
   </div>

<!--27-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-top" src="img/tech26.jpg" alt="image Can't load" width="100%" height="200px">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
    <a data-toggle="modal" data-target="#exampleDeck-z" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-z" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-z" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-z" >Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
  Here’s why I think it works:
  No-bake. Perfect for summer.
  Very easy.
  Peanut butter and chocolate, duh.
  A good mixture of textures: creamy, firm, crumbly, crunchy.
  A fairy dusting of espresso powder to the crust, adding necessary depth and richness that anchors the whole thing.
  To me, the espresso powder is really where the magic lies. In normal life, I try to stay away from caffeine, but this is different. This is not espresso for the sake of buzzing your brain. This is espresso for unmatched FLAVOR. I’m telling you – a few tablespoons of DeLallo Instant Espresso Powder (my secret baking weapon of choice) added to the crust just gives it that little extra somethin’ somethin’. Everyone who tried the pie commented on their love of the crust. It’s me, but it’s not just me.
  LET’S MAKE A MOCHA PEANUT BUTTER PIE:
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
    </div>
  </div>


<!--Row 10-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--28-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-top" src="img/tech27.jpg" alt="image Can't load" width="100%" height="200px">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
          <a data-toggle="modal" data-target="#exampleDeck-aa" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
 <div class="modal fade" id="exampleDeck-aa" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-aa" aria-hidden="true">
 <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-aa" >Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
 <blockquote>
   This is Mocha Peanut Butter Pie, and THIS IS YOUR WARNING.
 Stop here. Proceed with caution.
 If you don’t want to lick beaters, if you don’t want to have a reason to snack on a few hundred peanut butter cups, if you don’t want to find yourself standing in front of the fridge at 11pm feasting on broken chunks of mocha cookie crust, then you need to find yourself another pie.
 This pie has:
 a creamy peanut butter cheesecake layer;
 a crushed chocolate cookie crust flavored with espresso;
 a top layer of creamy chocolate ice cream;
 chunks of peanut butter cups ribboned throughout;
 a complete and utter hold on my life.
 Yeah. This pie is for people who are SERIOUS about their dessert.
 </blockquote>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
  </div>
 </div>
 </div>
      </div>

   <!--29-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-top" src="img/tech28.jpg" alt="image Can't load" width="100%" height="200px">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
    <a data-toggle="modal" data-target="#exampleDeck-ab" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
<div class="modal fade" id="exampleDeck-ab" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-ab" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
 <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-ab" >Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
 </div>
 <div class="modal-body">
<blockquote>
It’s an oil-and-vinegar dressing that you can shake up in a jar, sweetened up with a little sugar, seasoned with a little sesame oil, and… if you are me… creamified with a dollop of Greek yogurt. I don’t know, for me, a vinaigrette-drenched salad just isn’t going to keep me full. I need a little more fat happening in my salads, so I just took my favorite retro dressing (shout out to Kim Harcey of the church cookbook) and creamied it up with that whole milk yogurt, or, even on some occasions, mayo.For the vegans – leave out the creamy stuff!
For the gluten-free-ers – leave out the retro crunchy chow mein noodles!
For the protein-lovers – add some grilled chicken! Or shrimp! Or… I don’t know, tofu?! This quickly becomes a dinner entree salad with juicy grilled chicken sliced across the top.
</blockquote>
 </div>
 <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
 </div>
</div>
</div>
</div>
   </div>

<!--30-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-top" src="img/tech29.jpg" alt="image Can't load" width="100%" height="200px">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
  <a data-toggle="modal" data-target="#exampleDeck-ac" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-ac" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-ac" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-ac" >Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
  This salad is checking all the boxes for me these days.
  Lunch rolls around and my internal conversation goes like this:
  I’m hungry.
  What would taste awesome right now?
  No, wait, what would be super healthy right now? Because… mocha peanut butter pie?
  No, but what would actually fill me up?
  WAIT WAIT WAIT what about that salad with the sesame dressing that is awesome-tasting AND healthy AND keeps me full because it’s full of so many friendly fats and good stuff?
  Yes! I love salad!
  I love vegetables!
  I love life!
  That’s an actual transcript of my internal dialogue. These are the terms I think in: tastes amazing, mostly healthy, still fills me up.
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
    </div>
  </div>

<!--Row 11-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--31-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-top" src="img/tech1.jpg" alt="image Can't load" width="100%" height="200px">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
          <a data-toggle="modal" data-target="#exampleDeck-ad" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
 <div class="modal fade" id="exampleDeck-ad" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-ad" aria-hidden="true">
 <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-ad" >Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
 <blockquote>
   Yeah, like if you haven’t done this yet, you haven’t really lived.
   There are lots of other elements to this recipe – pineapple salsa,
    slaw, avocado mash, and chipotle mayo – and to me, all of them are necessary.
   What is the opposite of a taco minimalist? That is me. I want it all. Let’s do it all up.
   But really, even though there’s a lot going on, it doesn’t take long. You know I have zero time for impractical food prep
   . While your cauliflower walnut taco meat bakes up, you are prepping all the stuffs. No biggie.
   Then, you are stuffing the stuffs in a tortilla. With more stuffs.
 </blockquote>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
  </div>
 </div>
 </div>
      </div>

<!--32-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-top" src="img/tech2.jpg" alt="image Can't load" width="100%">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
      <a data-toggle="modal" data-target="#exampleDeck-ae" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
<div class="modal fade" id="exampleDeck-ae" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-ae" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
 <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-ae" >Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
 </div>
 <div class="modal-body">
<blockquote>
  Okay, here’s a bonus: you can buy all these ingredients at ALDI. No joke, I added up the cost
   of my ALDI taco ingredients and compared it to the cost of the ingredients from a different grocery store.
  Other store: $48.14
  ALDI: $24.44
  HELLO MONEY IN MY POCKET. Let’s buy all our stuff at ALDI – juicy fresh pineapple and jumbo bag of would-have-been expensive walnuts included!
  THE MAKING OF VEGAN WALNUT CHORIZO TACOS
  Alright, magical times ahead.
  Cauliflower, walnuts, chipotles, and a pinch of spices. Pulse, bake, and you’re done.
</blockquote>
 </div>
 <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
 </div>
</div>
</div>
</div>
   </div>

<!--33-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-top" src="img/tech3.jpg" alt="image Can't load" width="100%" height="200px">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
    <a data-toggle="modal" data-target="#exampleDeck-af" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-af" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-af" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-af" >Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
  Today at the party we have:
  Smoky walnut cauliflower chorizo (honestly though, the texture! it’s crumbly and flavorful and delicious just like chorizo).
  Juicy-sweet pineapple salsa loaded with jalapeno, lime, and cilantro.
  Avocado mash, quick purple cabbage slaw, and a nice swish of chipotle mayo on top, all tucked into a roasted tortilla.
  How about a big ol’ Y-E-S to that?
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
</div>
   </div>
 </div>


<div class="container" style="padding-left:100px;">
  <hr>
</div>

                              <!--Post end-->


<br>
<br>
<br>


 <!--Pager-->
<div class="container" style="margin-right:150px;margin-bottom:70px;">
  <ul class="breadcrumb bg-white justify-content-end">
    <li class="breadcrumb-item active">Prev</li>
    <li class="breadcrumb-item"><a href="#" class="success-link">1</a></li>
    <li class="breadcrumb-item"><a href="#" class="success-link">2</a></li>
    <li class="breadcrumb-item"><a href="#" class="success-link">3</a></li>
    <li class="breadcrumb-item"><a href="#" class="success-link">4</a></li>
    <li class="breadcrumb-item"><a href="#" class="success-link">Next</a></li>
  </ul>
</div>
<!--Pager-->


<!--Foooter-->
<footer>
  <div class="jumbotron jumbotron-fluid bg-dark" style="padding-bottom:10px;padding-top:-10px;">
     <!--footer links-->
     <div class="container text-center text-md-left">
        <!--Grid row-->
        <div class="row">
          <!--Grid Columns-->
          <div class="col-md-6 mx-auto text-white">
            <!--Content-->
            <h5 class="font-weight-bold text-uppercase mt-3 mb-4">About Devmins</h5>
            <p class="text-white">Devmins is simply awesome. With lots of features and elegant themes to choose from, it leads the blogging front.	</p>
          </div>
          <!--Grid columns-->
          <hr class="clearfix w-100 d-md-none">
          <div class="col-md-3 mx-auto">
            <!--Links-->
            <h5 class="font-weight-bol text-uppercase mt-3 mb-4 text-white">Useful links</h5>
            <ul class="list-unstyled text-white">
              <li>
                <a href="#" class="text-white">Your Account</a>
              </li>
              <li>
                <a href="#" class="text-white">Become Affiliate</a>
              </li>
              <li>
                <a href="#" class="text-white">Help</a>
              </li>
              <li>
                <a href="#" class="text-white">About Us</a>
              </li>
              <li>
                <a href="#" class="text-white">Know More</a>
              </li>
            </ul>
          </div>
          <!--Grid columns-->
          <hr class="clearfix w-100 d-md-none">
          <!-- Grid column -->
<div class="col-md-3 mx-auto">
  <h6 class="font-weight-bol text-uppercase mt-3 mb-4 text-white">Contact</h6>
    <ul class="list-unstyled text-white">
      <li><i class="fa fa-home mr-3"></i>G224, 4th Street ,Block G, Gamma-2, Greater Noida, U.P-201308</li>
      <li><i class="fa fa-envelope mr-3"></i> sarvesh@gmail.com</li>
      <li><i class="fa fa-phone mr-3"></i> + 91 96 547 914 68</li>
      <li><i class="fa fa-print mr-3"></i> + 01 99 115 859 87</li>
    </ul>
<!-- Grid column -->
        </div>
     </div>
     <hr>
  <!-- Social buttons -->
<ul class="list-inline text-center">
 <li class="list-inline-item">
   <a class="">
     <i class="fa fa-facebook"> </i>
   </a>
 </li>
 <li class="list-inline-item">
   <a class="">
     <i class="fa fa-twitter"> </i>
   </a>
 </li>
 <li class="list-inline-item">
   <a class="">
     <i class="fa fa-google"> </i>
   </a>
 </li>
 <li class="list-inline-item">
   <a class="">
     <i class="fa fa-linkedin"> </i>
   </a>
 </li>
 <li class="list-inline-item">
   <a class="">
     <i class="fa fa-instagram"> </i>
   </a>
 </li>
</ul>
<!-- Social buttons -->
<hr>

<!--Copyright-->
<div class="">
<div class="footer-copyright text-center text-muted" style="padding-bottom:50px;">@ 2018 Copyright:
 <a href="https://devmins.com" class="text-white">Devmins.com</a>
</div>
<!--Copyright-->
  </div>
</div>
 </footer>
</footer>
   <!--Footer end-->
          </body>
        </html>
