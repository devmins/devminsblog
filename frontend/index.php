<?php
/**
 * Created by PhpStorm.
 * User: Nishant
 * Date: 02-06-2018
 * Time: 12:45
 */
include 'header.php';
include 'navbar.php';
?>
<!--carousel start-->
    <!--indicators-->
<div id="demo" class="carousel slide" data-ride="carousel">
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
    <li data-target="#demo" data-slide-to="3"></
    <li data-target="#demo" data-slide-to="4"></li>
    <li data-target="#demo" data-slide-to="5"></li>
    <li data-target="#demo" data-slide-to="6"></li>
  </ul>

  <!-- The Slideshow-->
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="img/travel.jpg" alt="Travel blogs" class="img-fluid" width="1100" style="height:500px">
      <div class="carousel-caption">
        <h3 class="carousel-h3">Travel</h3>
        <p class="carousel-p">inpiration for travels a lot</p>
      </div>
    </div>

    <div class="carousel-item">
      <img src="img/business.jpg" alt="Business blog" class="img-fluid" width="1100" style="height:500px">
      <div class="carousel-caption">
        <h3 class="carousel-h3">Business</h3>
        <p class="carousel-p">meeting proposal</p>
      </div>
    </div>

    <div class="carousel-item">
      <img src="img/food.jpg" alt="Food blogs" class="img-fluid" width="1100" style="height:500px">
      <div class="carousel-caption">
        <h3 class="carousel-h3">Food</h3>
        <p class="carousel-p">eat as much as you can eat.</p>
      </div>
    </div>

    <div class="carousel-item">
      <img src="img/lifestyle.jpg" alt="lifestyle blogs" class="img-fluid" width="1100" style="height:500px">
      <div class="carousel-caption">
        <h3 class="carousel-h3">Lifestyle</h3>
        <p class="carousel-p">Dress like a professional</p>
      </div>
    </div>

    <div class="carousel-item">
      <img src="img/social.jpg" alt="social blog" class="img-fluid" width="1100" style="height:500px">
      <div class="carousel-caption">
        <h3 class="carousel-h3">Social Media</h3>
        <p class="carousel-p">Better to communicate</p>
      </div>
    </div>

   <div class="carousel-item">
  <img src="img/tech.jpg" alt="technology blogs" class="img-fluid" width="1100" style="height:500px">
  <div class="carousel-caption">
    <h3 class="carousel-h3"> Technology</h3>
    <p class="carousel-p">upcoming technology that changes world</p>
  </div>
  </div>

  <div class="carousel-item">
    <img src="img/coding.jpg" alt="coding challenges" class="img-fluid" width="1100" style="height:500px">
    <div class="carousel-caption">
      <h3 class="carousel-h3">Coding</h3>
      <p class="carousel-p">coding for technology.</p>
    </div>
  </div>
  </div>

  <!--left and right controls-->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
<br>
<br>

       <!--Trending posts-->
        <div class="container">
           <h2 class="text-dark text-center">Trending posts</h2>
        </div>
    <br>
  <br>

      <!--posts on wall-->
          <div class="container">
            <div class="card-deck">

              <!--Post1-->
              <div class="card shadow" style="width:300px;margin-bottom:120px;">
                <!--Card header-->
             <div class="card-header" style="background:white !important;">
               <div class="row" style="margin-left:0px">
                 <div class="col-md-6">
                   <b class="float-left">Jennifer</b>
                 </div>
                 <div class="col-md-6">
                   <a href="#" class="text-primary float-right">Follow</a>
                 </div>
               </div>
             </div>
                 <img class="card-img-top" src="img/tech.jpg" alt="card-image" style="width:100%">
                       <div class="card-body">
                    <h4 class="card-title text-secondary text-center">AI strategy</h4>
                 <p class="card-text">utting the intelligence in AI infrastructure From Watson™ to
                           the next generation of analytics and open source machine
                           learning,IBM brings you the AI infrastructure.</p>
               <a class="text-primary"href="#">Read more</a>
               </div>
             </div>

             <!--Post 2-->
             <div class="card shadow" style="width:300px;margin-top:30px;margin-bottom:60px;">
            <div class="card-header" style="background:white !important;">
              <div class="row">
                <div class="col-md-6">
                  <b class="float-left">Zayn</b>
                </div>
                <div class="col-md-6">
                  <a href="#" class="text-primary float-right">Follow</a>
                </div>
              </div>
            </div>
                <img class="card-img-top" src="img/coding.jpg" alt="card-image" style="width:100%;">
                      <div class="card-body">
                   <h4 class="card-title text-secondary text-center">Javascript</h4>
                <p class="card-text">Despite the level of criticism that we face from year to year,
                JavaScript is still one of the fastest-growing programming languages in the world.
                Besides that, according to the data gathered by the</p>
              <a class="text-primary"href="#">Read more</a>
              </div>
            </div>


            <!--Post3-->
            <div class="card shadow" style="width:300px;">
              <div class="card-header" style="background:white !important;">
                <div class="row">
                  <div class="col-md-6">
                    <b class="float-left">Ketty</b>
                  </div>
                  <div class="col-md-6">
                    <a href="#" class="text-primary float-right">Follow</a>
                  </div>
                </div>
              </div>
               <img class="card-img-top" src="img/food.jpg" alt="card-image" style="width:100%">
                     <div class="card-body">
                  <h4 class="card-title text-secondary text-center">Minimalist Baker</h4>
               <p class="card-text">Few things are more disappointing than finding what
                  seems like the perfect recipe only to click and learn that it has more
                  than 20 ingredients, several of which you've never heard of. True to its name,
                 Minimalist Baker keeps things simple by sticking to 10 </p>
             <a class="text-primary"href="#">Read more</a>
             </div>
           </div>


           <!--Post4-->
           <div class="card shadow" style="width:300px;margin-top:30px;margin-bottom:40px;">
          <div class="card-header" style="background:white !important;">
            <div class="row">
              <div class="col-md-6">
                <b class="float-left">selena</b>
              </div>
              <div class="col-md-6" style="text-align-last:right">
                <a href="#" class="text-primary float-right" align="right">Follow</a>
              </div>
            </div>
          </div>
              <img class="card-img-top" src="img/social.jpg" alt="card-image" style="width:100%">
                    <div class="card-body">
                 <h4 class="card-title text-secondary text-center">Game changing Instagram</h4>
              <p class="card-text">nstagram is becoming more favorable for sellers, brands
                and businesses alike. Advertising on the platform is skyrocketing
                and new business features are truly changing the game..</p>
            <a class="text-primary"href="#">Read more</a>
            </div>
          </div>
        </div>
      </div>

         <!--Second deck-->
         <div class="container">
           <div class="card-deck">
           <!--Post5-->
           <div class="card shadow" style="width:300px;margin-top:-80px;margin-bottom:600px;">
           <div class="card-header" style="background:white !important;">
            <div class="row">
              <div class="col-md-6">
                <b class="float-left">martin</b>
              </div>
              <div class="col-md-6">
                <a href="#" class="text-primary float-right" align="right">Follow</a>
              </div>
            </div>
           </div>
              <img class="card-img-top" src="img/travel.jpg" alt="card-image" style="width:100%">
                    <div class="card-body">
                 <h4 class="card-title text-secondary text-center">Isolate Qatar By Physically Turning It Into An Island</h4>
              <p class="card-text">12 months ago, several Arab countries cut diplomatic
                ties with Qatar, accusing them of destabilising the region, by backing
               militant groups including the Islamic State (IS) and Al-Qaeda. These countries included:
                -- Saudi Arabia -- Bahrain -- United…</p>
            <a class="text-primary"href="#">Read more</a>
            </div>
           </div>


            <!--Post6-->
        <div class="card shadow" style="width:300px;margin-top:-15px;margin-bottom:440px;">
          <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-6">
                 <b class="float-left">Shawn</b>
                   </div>
                <div class="col-md-6" style="text-align-last:right">
                   <a href="#" class="text-primary float-right" align="right">Follow</a>
                 </div>
            </div>
          </div>
         <img class="card-img-top" src="img/lifestyle.jpg" alt="card-image" style="width:100%">
     <div class="card-body">
        <h4 class="card-title text-secondary text-center">A Healthy Life Well Styled.</h4>
          <p class="card-text">Discover a fresh approach to style
            through fun ideas for the wardrobe and home. Try delicious
              market-fresh recipes that are perfect for dinner tonight. Take
           advice from inspiring influencers on health, fitness, fashion,
         beauty, and professional goals at a site that views every aspect of
       clean living through a glamorous lens.Frequency about 19 posts per week. </p>
     <a class="text-primary"href="#">Read more</a>
    </div>
    </div>


              <!--Post7-->
        <div class="card shadow" style="width:300px;margin-top:40px;margin-bottom:300px;">
          <div class="card-header" style="background:white !important;">
            <div class="row">
              <div class="col-md-6">
                 <b class="float-left">Kygo</b>
                   </div>
                 <div class="col-md-6" style="text-align-last:right">
                  <a href="#" class="text-primary float-right" align="right">Follow</a>
                 </div>
             </div>
          </div>
           <img class="card-img-top" src="img/food3.jpg" alt="card-image" style="width:100%">
              <div class="card-body">
                  <h4 class="card-title text-secondary text-center">Green Kitchen</h4>
                     <p class="card-text">The globe-trotting family behind Green Kitchen Stories
                        is hardly stuck on one type of cuisine. David and Luise met while studying in
                         Rome before moving to Stockholm to start a family. They document their vegetarian
                          cooking experiments on the blog, and they believe variety is the most important thing
                          in a diet. Don't forget to follow them both on Instagram; David's and Luise's
                       accounts will inspire you to live and eat better every day.</p>
                       <a class="text-primary"href="#">Read more</a>
             </div>
          </div>


          <!--Post8-->
    <div class="card shadow" style="width:300px;margin-bottom:300px;">
      <div class="card-header" style="background:white !important;">
        <div class="row">
          <div class="col-md-6">
             <b class="float-left">Pitbull</b>
               </div>
             <div class="col-md-6">
              <a href="#" class="text-primary float-right" align="right">Follow</a>
             </div>
         </div>
      </div>
       <img class="card-img-top" src="img/coding6.jpg" alt="card-image" style="width:100%">
          <div class="card-body">
              <h4 class="card-title text-secondary text-center">U.S. reclaims top spot for world’s fastest supercomputer</h4>
                 <p class="card-text">TOP500 released an update to its list of the fastest
                    supercomputers in the world, with the U.S. Department of Energy’s Oak Ridge
                    National Laboratory leading the way. In its debut earlier this month, Summit
                     clocked in at 122 petaflops of compute power on High Performance Linpack (HPL), a benchmark
                   used to rank supercomputers ranked on the TOP500 list.</p>
                   <a class="text-primary"href="#">Read more</a>
               </div>
             </div>
          </div>
        </div>


              <!--Third deck-->
                  <div class="container">
                    <div class="card-deck">

                      <!--Post9-->
                      <div class="card shadow" style="width:300px;margin-bottom:550px;margin-top:-550px;">
                        <!--Card header-->
                     <div class="card-header" style="background:white !important;">
                       <div class="row" style="margin-left:0px">
                         <div class="col-md-6">
                           <b class="float-left">Taylor</b>
                         </div>
                         <div class="col-md-6">
                           <a href="#" class="text-primary float-right">Follow</a>
                         </div>
                       </div>
                     </div>
                         <img class="card-img-top" src="img/coding3.jpg" alt="card-image" style="width:100%">
                               <div class="card-body">
                            <h4 class="card-title text-secondary text-center">Resilient, Declarative, Contextual</h4>
                         <p class="card-text">I want to look at three key characteristics of CSS that set it
                           apart from conventional programming languages: it’s resilient; it’s declarative; and it’s contextual. Understanding
                           these aspects of the language, I think, is key to becoming proficient in CSS.</p>
                       <a class="text-primary"href="#">Read more</a>
                       </div>
                     </div>

                     <!--Post 10-->
                     <div class="card shadow" style="width:300px;margin-top:-400px;margin-bottom:170px;">
                    <div class="card-header" style="background:white !important;">
                      <div class="row">
                        <div class="col-md-6">
                          <b class="float-left">james</b>
                        </div>
                        <div class="col-md-6">
                          <a href="#" class="text-primary float-right">Follow</a>
                        </div>
                      </div>
                    </div>
                        <img class="card-img-top" src="img/food.jpg" alt="card-image" style="width:100%;">
                              <div class="card-body">
                           <h4 class="card-title text-secondary text-center">Cookbooks</h4>
                        <p class="card-text">Back in 2003, Heidi Swanson launched 101 Cookbooks with the goal
                              of cooking through her monstrous collection of recipe books. More than 10 years
                              later, the blog (which we want to live in, it's so gorgeous) focuses on natural,
                                  whole foods and Heidi's travels. Naturally it has won multiple awards, been
                            featured in countless publications, and even turned into two bestselling cookbooks.
                            Her latest, Near & Far: Recipes Inspired by Home and Travel, came out in late 2015.</p>
                      <a class="text-primary"href="#">Read more</a>
                      </div>
                    </div>


                    <!--Post 11-->
                    <div class="card shadow" style="width:300px;margin-top:-260px;margin-bottom:240px;">
                      <div class="card-header" style="background:white !important;">
                        <div class="row">
                          <div class="col-md-6">
                            <b class="float-left">Hendrix</b>
                          </div>
                          <div class="col-md-6">
                            <a href="#" class="text-primary float-right">Follow</a>
                          </div>
                        </div>
                      </div>
                       <img class="card-img-top" src="img/coding4.jpg" alt="card-image" style="width:100%">
                             <div class="card-body">
                          <h4 class="card-title text-secondary text-center">Drawing Images with CSS Gradients</h4>
                       <p class="card-text">What I mean by "CSS images" is images that are created using only HTML elements and CSS.
                         They look as if they were SVGs drawn in Adobe Illustrator but they were made right in the browser.
                          Some techniques I’ve seen used are tinkering with border radii, box shadows, and sometimes clip-path. </p>
                     <a class="text-primary"href="#">Read more</a>
                     </div>
                   </div>


                   <!--Post12-->
                   <div class="card sha" style="width:300px;margin-top:-250px;margin-bottom:30px;">
                  <div class="card-header" style="background:white !important;">
                    <div class="row">
                      <div class="col-md-6">
                        <b class="float-left">Bandit</b>
                      </div>
                      <div class="col-md-6" style="text-align-last:right">
                        <a href="#" class="text-primary float-right" align="right">Follow</a>
                      </div>
                    </div>
                  </div>
                      <img class="card-img-top" src="img/food5.jpg" alt="card-image" style="width:100%">
                            <div class="card-body">
                         <h4 class="card-title text-secondary text-center">Green Kitchen stories</h4>
                      <p class="card-text">The globe-trotting family behind Green Kitchen Stories is hardly
                        stuck on one type of cuisine. David and Luise met while studying in Rome before moving
                         to Stockholm to start a family. They document their vegetarian cooking experiments on
                         the blog, and they believe variety is the most important thing in a diet. Don't forget
                          to follow them both on Instagram;
                        David's and Luise's accounts will inspire you to live and eat better every day.</p>
                    <a class="text-primary"href="#">Read more</a>
                    </div>
                  </div>
                </div>
              </div>

          <!--fifth deck-->
          <div class="container">
            <div class="card-deck" style="width:300px">
              <div class="card shadow" style="width:300px;margin-top:-520px;margin-bottom:30px;">
             <div class="card-header" style="background:white !important;">
               <div class="row">
                 <div class="col-md-6">
                   <b class="float-left">selena</b>
                 </div>
                 <div class="col-md-6" style="text-align-last:right">
                   <a href="#" class="text-primary float-right">Follow</a>
                 </div>
               </div>
             </div>
                 <img class="card-img-top" src="img/food5.jpg" alt="card-image" style="width:260px;height:100px">
                       <div class="card-body">
                    <h4 class="card-title text-secondary text-center">THE NORTHERN LIGHTS</h4>
                 <p class="card-text">I shared a little about our recent vacation to Norway earlier this week,
                   so if you read that then you already know why</p>
               <a class="text-primary" href="#">Read more</a>
               </div>
             </div>
                </div>
              </div>
            </div>
          </div>

<?php
include 'footer.php';
?>
