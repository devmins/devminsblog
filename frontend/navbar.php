<?php
/**
 * Created by PhpStorm.
 * User: Nishant
 * Date: 02-06-2018
 * Time: 12:50
 */?>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand" href="index.php">DevMins</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <!--search in navbar-->
        
        <!--options in navbar-->
          <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="index.php">Home</a>
              </li>

             <li class="nav-item dropdown">
               <a class="nav-link" href="explore.php">Explore</a>
             </li>

             <li class="nav-item">
               <a class="nav-link" href="contact.php">Inbox</a>
             </li>

            <li class="nav-item">
              <a class="nav-link" href="post.php">Messaging</a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="#">Activity</a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="#">Account</a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="#">Post</a>
            </li>
          </ul>
        </div>
    </div>
</nav>
