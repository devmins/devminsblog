<?php
/**
 * Created by PhpStorm.
 * User: Nishant
 * Date: 02-06-2018
 * Time: 12:51
 */
?>
<hr>

<!-- Footer -->
       <footer>
        <div class="container-fluid bg-dark">
           <!--footer links-->
           <div class="container text-center text-md-left">


              <!--Grid row-->
              <div class="row">
                <!--Grid Columns-->

                <div class="col-md-6 mx-auto text-white">
                  <!--Content-->
                  <h5 class="font-weight-bold text-uppercase mt-3 mb-4">About Devmins</h5>
                  <p class="text-white">Devmins is simply awesome. With lots of features and elegant themes to choose from, it leads the blogging front.	</p>
                </div>

                <!--Grid columns-->
                <hr class="clearfix w-100 d-md-none">
                <div class="col-md-3 mx-auto">
                  <!--Links-->
                  <h5 class="font-weight-bol text-uppercase mt-3 mb-4 text-white">Useful links</h5>
                  <ul class="list-unstyled text-white">
                    <li>
                      <a href="#" class="text-white">Your Account</a>
                    </li>
                    <li>
                      <a href="#" class="text-white">Become Affiliate</a>
                    </li>
                    <li>
                      <a href="#" class="text-white">Help</a>
                    </li>
                    <li>
                      <a href="#" class="text-white">About Us</a>
                    </li>
                    <li>
                      <a href="#" class="text-white">Know More</a>
                    </li>
                  </ul>
                </div>


                <!--Grid columns-->
                <hr class="clearfix w-100 d-md-none">
                <!-- Grid column -->
      <div class="col-md-3 mx-auto">
        <h6 class="font-weight-bol text-uppercase mt-3 mb-4 text-white">Contact</h6>
          <ul class="list-unstyled text-white">
            <li><i class="fa fa-home mr-3"></i>G224, 4th Street ,Block G, Gamma-2, Greater Noida, U.P-201308</li>
            <li><i class="fa fa-envelope mr-3"></i> sarvesh@gmail.com</li>
            <li><i class="fa fa-phone mr-3"></i> + 91 96 547 914 68</li>
            <li><i class="fa fa-print mr-3"></i> + 01 99 115 859 87</li>
          </ul>
      <!-- Grid column -->
              </div>
           </div>

           <hr>


        <!-- Social buttons -->
     <ul class="list-inline text-center">
       <li class="list-inline-item">
         <a class="">
           <i class="fa fa-facebook"> </i>
         </a>
       </li>
       <li class="list-inline-item">
         <a class="">
           <i class="fa fa-twitter"> </i>
         </a>
       </li>
       <li class="list-inline-item">
         <a class="">
           <i class="fa fa-google"> </i>
         </a>
       </li>
       <li class="list-inline-item">
         <a class="">
           <i class="fa fa-linkedin"> </i>
         </a>
       </li>
       <li class="list-inline-item">
         <a class="">
           <i class="fa fa-instagram"> </i>
         </a>
       </li>
     </ul>
     <!-- Social buttons -->
      <hr>

     <!--Copyright-->
     <div class="">
     <div class="footer-copyright text-center text-muted" style="padding-bottom:50px;">@ 2018 Copyright:
       <a href="https://devmins.com" class="text-white">Devmins.com</a>
     </div>
     <!--Copyright-->
        </div>
      </div>
       </footer>
       </body>

     </html>
