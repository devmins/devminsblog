<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
    <title>Devmins</title>

    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>

.fa {
  padding: 6px ;
  font-size: 36px;
  width: 48px;
  height: 50px
  text-align: center !important;
  text-decoration: none;
  margin: 5px 2px;
  border-radius: 50%;
}

.fa:hover {
    opacity: 0.7;
}

.fa-facebook {
  background: #3B5998;
  color: white;
}

.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-google {
  background: #dd4b39;
  color: white;
}

.fa-linkedin {
  background: #007bb5;
  color: white;
}

.fa-instagram {
  background: #125688;
  color: white;
}

</style>
  </head>
  <body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-md  navbar-dark fixed-top" style="padding-bottom:10px;padding-top:10px;background-color:	#505050 ;" >
        <div class="container">
            <a class="navbar-brand" href="index.php">DevMins</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
              <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="index.php"><b>Home</b></a>
                  </li>

                 <li class="nav-item dropdown">
                   <a class="nav-link" href="explore.php"><b>Explore</b></a>
                 </li>

                 <li class="nav-item">
                   <a class="nav-link" href="contact.php"><b>Inbox</b></a>
                 </li>

                <li class="nav-item">
                  <a class="nav-link" href="post.php"><b>Messaging</b></a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#"><b>Activity</b></a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#"><b>Account</b></a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#"><b>Post</b></a>
                </li>
              </ul>
            </div>
        </div>
    </nav>
    <!--Navbar-->
<br>
<br>
<br>
<br>


<!--Categories-->
<div class="container-fluid">
 <ul class="nav justify-content-center">
   <li class="nav-item">
     <a class="nav-link" href="explore.php"><b>FOOD</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="coding.php"><b>CODING</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="tech.php"><b>TECH</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="lifestyle.php"><b>LIFESTYLE</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="design.php"><b>DESIGN</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="science.php"><b>SCIENCE</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="media.php"><b>MEDIA</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="politics.php"><b>POLITICS</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="self.php"><b>SELF</b></a>
   </li>
   <li class="nav-item">
     <a class="nav-link" href="more.php"><b>MORE</b></a>
   </li>
   <!--Categories-->
<br>
<br>
<br>

      <div class="container" style="margin-left:190px;margin-top:50px";>
        <h1>Coding</h1>
        <h6>First Solve the Problem.Then Write the Code.</h6>
        <br>
      </div>


<!--Posts-->

  <!--Deck1-->
    <div class="container" style="padding-left:100px;">
      <hr>
      <!--1-->
        <div class="card-columns" style="margin-left:10px;">
           <div class="card" style="width:320px";>
             <div class="card-header" style="background:white !important;">
               <div class="row">
                 <div class="col-md-2">
                   <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
                 </div>
                 <div class="col-md-5">
                   <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                   <span style="font-size:10px;margin-left:4px !important">harison96</span>
                 </div>
                 <div class="col-md-5">
                   <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
                 </div>
               </div>
             </div>
                 <img class="card-img-left" src="img/coding.jpg" alt="image Can't load" height="200px" width="100%">
                 <div class="card-body">
              <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
            <a data-toggle="modal" data-target="#exampleDeck-A" class="text-primary">Read More</a>
          </div>
          <!-- Modal -->
<div class="modal fade" id="exampleDeck-A" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-A" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-A" >Minimalist Baker</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<blockquote>
Norway’s Equinor is betting on digitalization to help it not only improve
safety and reduce its carbon footprint of its operations, but
 also increase revenue by $2 billion, reduce offshore drilling costs by about 15%,
 lower future investment by about 30% compared to traditional oil
 and gas development and improve safety performance among other goals,
 according to Torbjørn Folgerø, chief digital officer for Equinor.
</blockquote>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</div>


      <!--2-->
        <div class="card" style="width:320px";>
          <div class="card-header" style="background:white !important;">
            <div class="row">
              <div class="col-md-2">
                <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
              </div>
              <div class="col-md-5">
                <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                <span style="font-size:10px;margin-left:4px !important">harison96</span>
              </div>
              <div class="col-md-5">
                <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
              </div>
            </div>
          </div>
              <img class="card-img-left" src="img/coding1.jpg" alt="image Can't load" height="200px" width="100%">
              <div class="card-body">
           <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
         <a data-toggle="modal" data-target="#exampleDeck-B" class="text-primary">Read More</a>
       </div>
       <!-- Modal -->
<div class="modal fade" id="exampleDeck-B" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-B" aria-hidden="true">
<div class="modal-dialog" role="document">
 <div class="modal-content">
   <div class="modal-header">
     <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-B" >Minimalist Baker</h5>
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
       <span aria-hidden="true">&times;</span>
     </button>
   </div>
   <div class="modal-body">
<blockquote>
  STON—The energy industry has amassed tons of data from its upstream
   operations via sensors on equipment with data accessible through the
   cloud, but it’s how the data is used that adds value, according to speakers at a recent digital technology event.
  Equinor (NYSE: EQNR), formerly known as Statoil, and Royal Dutch Shell
   (NYSE: RDS.A) were among the companies sharing insight on their digital technology efforts and strategy during UNIFY 2018,
  a digital-focused event put on by Baker Hughes, a GE company (NYSE: BHGE).
</blockquote>
   </div>
   <div class="modal-footer">
     <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
   </div>
 </div>
</div>
</div>
</div>


<!--3-->
     <div class="card" style="width:320px";>
       <div class="card-header" style="background:white !important;">
         <div class="row">
           <div class="col-md-2">
             <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
           </div>
           <div class="col-md-5">
             <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
             <span style="font-size:10px;margin-left:4px !important">harison96</span>
           </div>
           <div class="col-md-5">
             <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
           </div>
         </div>
       </div>
           <img class="card-img-left" src="img/coding2.jpg" alt="image Can't load" height="200px" width="100%">
           <div class="card-body">
        <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
       <a data-toggle="modal" data-target="#exampleDeck-C" class="text-primary">Read More</a>
    </div>
    <!-- Modal -->
<div class="modal fade" id="exampleDeck-C" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-C" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
  <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-C" >Minimalist Baker</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
<blockquote>
BHGE CEO Lorenzo Simonelli said 80% of all of the Internet of Things
deployments that will take place will be specific to certain problems.
He pointed out that many companies are looking at nonproductive time,
asset performance management and production optimization. But as other speakers pointed out,
Simonelli said it is time to move beyond proof of concept.
</blockquote>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
  </div>
</div>
</div>


<!--Deck2-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--4-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-left" src="img/coding3.jpg" alt="image Can't load" height="200px" width="100%">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
            <a data-toggle="modal" data-target="#exampleDeck-D" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
 <div class="modal fade" id="exampleDeck-D" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-D" aria-hidden="true">
 <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-D" >Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
 <blockquote>
Torbjørn Folgerø, chief digital officer for Equinor, was also among the speakers. The company is betting on
 digitalization to help it not only improve safety and reduce its carbon footprint of its operations,
 but also increase its revenue by $2 billion, reduce offshore drilling
/costs by about 15% and lower future investment by about 30%
compared to traditional oil and gas development among other goals.
 </blockquote>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
  </div>
 </div>
 </div>
      </div>


     <!--5-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-left" src="img/coding4.jpg" alt="image Can't load" height="200px" width="100%">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
         <a data-toggle="modal" data-target="#exampleDeck-E" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
<div class="modal fade" id="exampleDeck-E" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-E" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
 <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-E" >Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
 </div>
 <div class="modal-body">
<blockquote>
But getting there is may seem daunting given silos and different systems used. Paula Doyle, director of customer success
at Cognite, suggested breaking down silos by decoupling applications from source systems,
 eliminating the need to do point to point integration and expanding access to data.
 The company contextualizes all types of data on its platform. The company
is working with Aker BP, streaming 800,000 data points from its operations.
</blockquote>
 </div>
 <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
 </div>
</div>
</div>
</div>
   </div>


  <!--6-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-left" src="img/coding5.jpg" alt="image Can't load" height="200px" width="100%">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
      <a data-toggle="modal" data-target="#exampleDeck-F" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-F" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-F" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-F">Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
  “Technology is no longer the bottleneck; it is the enabler,” said Matthias Heilmann, president and CEO of digital solutions for BHGE.
  “If you start with the outcome you’ll see the benefit right in front of you.”
  However, the industry is still not using all of the data it has, according
   to Darryl Willis, vice president of oil, gas and energy for Google Cloud.
  “Unfortunately it is estimated that we only use about 5% of the data that
  we have at our disposal,” Willis said. “It is time that we do better. … Our
  challenge is the oil and gas industry is to not overstudy the problem or opportunity.
  He later added, “if you use your data, you win. If you don’t its very simple,
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>



<!--Deck3-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--7-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-left" src="img/coding6.jpg" alt="image Can't load" height="200px" width="100%">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
            <a data-toggle="modal" data-target="#exampleDeck-G" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
   <div class="modal fade" id="exampleDeck-G" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-G" aria-hidden="true">
   <div class="modal-dialog" role="document">
   <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-G" >Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
   <blockquote>
During the digital-focused event, billed by BHGE as an event for and by practitioners,
 the company spotlighted some of its digital products such as JewelSuite,
 a reservoir and well modeling software that is used by companies such as
 Royal Dutch Shell, and Intellistream, an AI-driven production optimization offering
   </blockquote>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
   </div>
   </div>
   </div>
      </div>


<!--8-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-left" src="img/coding7.jpg" alt="image Can't load" height="200px" width="100%">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
         <a data-toggle="modal" data-target="#exampleDeck-H" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
<div class="modal fade" id="exampleDeck-H" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-H" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
 <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-H">Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
 </div>
 <div class="modal-body">
<blockquote>
BHGE earlier this year teamed up with Nvidia to use AI to help optimize production
 and GPU-accelerated computing to analyze data such as pump pressures and flow rates that can give insight into possible problems.
 The oilfield service company—like many of its peers—also
 has teamed up with technology leaders such as Google Cloud, Microsoft,
Accenture and KBC to merge the energy and digital technology
 worlds and uncover fit-for-purpose solutions for the oil and gas industry’s challenges
</blockquote>
 </div>
 <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
 </div>
</div>
</div>
</div>
   </div>


<!--9-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-left" src="img/coding8.jpg" alt="image Can't load" height="200px" width="100%">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
      <a data-toggle="modal" data-target="#exampleDeck-I" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-I" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-I" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-I" >Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
  Bringing digital technologies such as artificial intelligence (AI)
  and digital twin models to the oil patch has moved digital technologies
  higher on the agendas of energy companies. The search for insight that
  could lead to operational improvements and other value additions—capitalizing
  on the abundance of data generated from oilfield equipment sensors and seismic, for starters—has
  become a priority as the industry plays catch up to other sectors
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<!--Deck4-->
  <div class="container" style="padding-left:100px;">
    <hr>

    <!--10-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-left" src="img/coding9.jpg" alt="image Can't load" height="200px" width="100%">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
            <a data-toggle="modal" data-target="#exampleDeck-J" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
   <div class="modal fade" id="exampleDeck-J" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-J" aria-hidden="true">
   <div class="modal-dialog" role="document">
   <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-J" >Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
   <blockquote>
“Imagine the power of tools like these,” he said, later showing how the technology can be used to run multiwell optimization on a cluster of
wells in 20 seconds—1,000 times faster than what’s available on the market today. “It’s not about a
specific model or specific optimization that you want to have; it’s about opening the
possibilities,” and enabling scalability—demonstrating how the same technology
 can be put to use for optimization at the field level with 500 wells
   </blockquote>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
   </div>
   </div>
   </div>
      </div>


    <!--11-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
             <img class="card-img-left" src="img/coding10.jpg" alt="image Can't load" height="200px" width="100%">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
         <a data-toggle="modal" data-target="#exampleDeck-K" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
<div class="modal fade" id="exampleDeck-K" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-K" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
 <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-K" >Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
 </div>
 <div class="modal-body">
<blockquote>
  Speaking to attendees of Baker Hughes, a GE company’s (NYSE: BHGE) UNIFY 2018
  event June 13 in Houston, Subramaniyan—the company’s vice president of data
  science and analytics—performed a live demonstration showing how a digital
  twin of a well in a field and artificial intelligence was used to
  pinpoint a calibration issue, which when solved help to optimize production.
  In a matter of five or six minutes, he went through three several scenarios—each
   of which would have taken three or four engineers a couple of weeks to do
</blockquote>
 </div>
 <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
 </div>
</div>
</div>
</div>
   </div>


<!--12-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-left" src="img/coding11.jpg" alt="image Can't load" height="200px" width="100%">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
      <a data-toggle="modal" data-target="#exampleDeck-L" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-L" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-L" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-L" >Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
And in the end, our gut will always be part of decision making.
 That gut instinct that speaks to you -- that tells you to try
  something, to ask a question, to do something differently -- it cannot necessarily correlate to a piece of data.
It can absolutely correlate with your future success
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<!--Deck4-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--13-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-left" src="img/coding12.jpg" alt="image Can't load" height="200px" width="100%">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
          <a data-toggle="modal" data-target="#exampleDeck-M" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
   <div class="modal fade" id="exampleDeck-M" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-M" aria-hidden="true">
   <div class="modal-dialog" role="document">
   <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-M" >Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
   <blockquote>
And in the end, our gut will always be part of decision making.
 That gut instinct that speaks to you -- that tells you to try something,
 to ask a question, to do something differently -- it cannot necessarily correlate to a piece of data.
It can absolutely correlate with your future success
   </blockquote>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
   </div>
   </div>
   </div>
      </div>


  <!--14-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-left" src="img/coding13.jpg" alt="image Can't load" height="200px" width="100%">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
         <a data-toggle="modal" data-target="#exampleDeck-N" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
<div class="modal fade" id="exampleDeck-N" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-N" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
 <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-N" >Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
 </div>
 <div class="modal-body">
<blockquote>
What I’m saying is that while an algorithm has the capacity
 to ask questions on a high level, only a human being is able
  to write those questions. The CEOs of the future will understand
  how to interpret results well and ask the next question,
and the question after that, and take that answer forward.
</blockquote>
 </div>
 <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
 </div>
</div>
</div>
</div>
   </div>


<!--15-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-left" src="img/coding14.jpg" alt="image Can't load" height="200px" width="100%">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
      <a data-toggle="modal" data-target="#exampleDeck-O" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-O" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-O" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-O" >Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
A machine can bring together disparate pieces of information into
a consumable, even actionable piece of information (sadly, glitches
happen, with a “computer glitch” costing Knight Capital $440 million).
However, it inherently is there to answer a question or create a series
of potential questions for you to ask it. Interpreting the results
of big data will soon become far more
important than what your big data startup’s algorithm can do.
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<!--Deck5-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--16-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-top" src="img/coding15.jpg" alt="image Can't load" height="200px" width="100%">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
            <a data-toggle="modal" data-target="#exampleDeck-P" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
   <div class="modal fade" id="exampleDeck-P" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-P" aria-hidden="true">
   <div class="modal-dialog" role="document">
   <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-P" >Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
   <blockquote>
And though an algorithm may be able to cover sports,
you cannot clone or generate whimsy or humor or the essence
 of what makes writing enjoyable to read. We are not (at least not yet)
 at a point where computers are able to have full conversations, let alone
  exude the creativity to come up with ideas. The creative geniuses of the
   future may, in fact, be aided by big data, but they will simply use
   it (as one would use Google to search the giant database known as the
   internet) to ask the right questions to solve the world’s problems.
   </blockquote>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
   </div>
   </div>
   </div>
      </div>


   <!--17-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-top" src="img/coding16.jpg" alt="image Can't load" height="200px" width="100%">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
          <a data-toggle="modal" data-target="#exampleDeck-Q" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
   <div class="modal fade" id="exampleDeck-Q" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-Q" aria-hidden="true">
   <div class="modal-dialog" role="document">
   <div class="modal-content">
   <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-Q" >Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
   </div>
   <div class="modal-body">
   <blockquote>
  Forbes Technology Council Elite CIOs, CTOs & execs offer firsthand insights on tech & business.
  Opinions expressed by Forbes Contributors are their own.
  POST WRITTEN BY
  Diego Fischer
  Diego Fischer is the co-founder and CEO of InstaCarro, a 150-person C2B company that enables users to sell used cars in 90 minutes for cash.
   Diego Fischer Diego Fischer , Forbes Councils
  Continued from page 1
  Don’t believe me? Amazon just opened up an entire consultancy service. TechCrunch’s Ingrid Lunden noted that this machine learning lab will pair "Amazon machine learning experts with customers looking to build solutions using the AI tech. And it’s releasing new features within Amazon Rekognition, Amazon’s deep learning-based image recognition platform:
  real-time face recognition and the ability to recognize text in images."
   </blockquote>
   </div>
   <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
   </div>
   </div>
   </div>
   </div>
   </div>


<!--18-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-top" src="img/coding17.jpg" alt="image Can't load" height="200px" width="100%">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
    <a data-toggle="modal" data-target="#exampleDeck-R" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-R" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-R" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-R" >Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
We are not far from the natural endpoint, where Amazon Web Services’
 algorithms can inherently build other algorithms to make sense of
  your big data in a click. In the same way that few stray from Amazon,
  Oracle, Microsoft or Google for their cloud storage, few will stray
  from the cloud-based AI number-crunchers that will
discern meaning for a fraction of the price that SaaS companies can.
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<!--Deck6-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--19-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-top" src="img/coding18.jpg" alt="image Can't load" height="200px" width="100%">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
          <a data-toggle="modal" data-target="#exampleDeck-S" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
      <div class="modal fade" id="exampleDeck-S" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-S" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-S">Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
      <div class="modal-body">
      <blockquote>
      I have a thesis: In five years, it’s not going to be possible
       to build a big data startup, as the Oracles, Amazons and Apples
        of the world will have commoditized it. Apple’s acquisition of a
        “dark data” (a ridiculous term referring to unstructured data) company
         for $200 million, Amazon’s $20 million purchase of machine learning and
          artificial intelligence security company harvest.ai and a whole host of other AI
      M&A's is the sign not of a boom but of a coming consolidation.
      </blockquote>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
      </div>
      </div>
      </div>
      </div>


   <!--20-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-top" src="img/coding19.jpg" alt="image Can't load" height="200px" width="100%">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
       <a data-toggle="modal" data-target="#exampleDeck-T" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
   <div class="modal fade" id="exampleDeck-T" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-T" aria-hidden="true">
   <div class="modal-dialog" role="document">
   <div class="modal-content">
   <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-T">Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
   </div>
   <div class="modal-body">
   <blockquote>
     Huang will succeed by powering the future. Those who simply know
     how to look at it and say, “Yep, machine learning is gonna change the world,
     and here’s an algorithm to spit out data” won’t be the kings of it.
     Let me be clear: Big data, machine learning and deep learning aren’t
     overrated on their own. It’s simply going to be a very different
     world soon -- one where the extremely analytical,
     overly lauded big data experts of the world won’t be quite as popular.
   </blockquote>
   </div>
   <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
   </div>
   </div>
   </div>
   </div>
   </div>


<!--21-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-top" src="img/coding20.jpg" alt="image Can't load" height="200px" width="100%">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
     <a data-toggle="modal" data-target="#exampleDeck-U" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-U" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-U" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-U">Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
Nvidia’s Huang is one of the few people who actually understand what deep learning is, if only because he’s turned his
 own hardware company into one that is prepared for its commoditization.
Graphics processing units -- which were usually reserved for high-tech gaming and visual
design -- now can number-crunch at an incredible speed
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>



<!--Deck8-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--22-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-top" src="img/coding21.jpg" alt="image Can't load" height="200px" width="100%">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
           <a data-toggle="modal" data-target="#exampleDeck-V" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
      <div class="modal fade" id="exampleDeck-V" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-V" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-U">Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
      <div class="modal-body">
      <blockquote>
      Barron's Tiernan Ray applauded Nvidia CEO Jen-Hsun Huang for giving "a shout out for deep learning" shortly before Ray
      categorized it as the "use of massive clusters of GPUs to teach computers new skills."
       Except that’s not the definition of deep learning according to several other sources,
       with some calling it “A subfield of machine learning concerned with algorithms
       inspired by the structure and function of the brain called artificial neural networks,”
       and Nvidia itself referring to it as a field of machine learning that "uses many-layered
       Deep Neural Networks (DNNs) to learn levels of representation and abstraction that make sense of data such as images, sound and text."
      </blockquote>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
      </div>
      </div>
      </div>
      </div>


  <!--23-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-top" src="img/coding22.jpg" alt="image Can't load" height="200px" width="100%">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
        <a data-toggle="modal" data-target="#exampleDeck-W" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
   <div class="modal fade" id="exampleDeck-W" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-W" aria-hidden="true">
   <div class="modal-dialog" role="document">
   <div class="modal-content">
   <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-W">Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
   </div>
   <div class="modal-body">
   <blockquote>
   Or on some level, you have to at least come off that way. We’re obsessed with finding new ways to
   mention new big data -- in our cars, our homes, our cities -- and investors even love the idea of
    building big data software for big data. We’re transitioning to a point where influential CEOs
    are given a great deal of credit for “big data initiatives.” Machine learning can spot patterns,
    it can tell you things, it can help create amazing products. The brightest CEOs are “seeing the
    Matrix” and discerning brilliant thoughts thanks to the mysterious power of a machine to learn and discern meaning
   </blockquote>
   </div>
   <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
   </div>
   </div>
   </div>
   </div>
   </div>

<!--24-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-top" src="img/coding23.jpg" alt="image Can't load" height="200px" width="100%">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
     <a data-toggle="modal" data-target="#exampleDeck-X" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-X" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-W" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-X">Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
We live in an era in which top executives can’t survive
without breaking into song about the wonders of big data.
They have to be able to create beautiful correlations of data,
 or at least hire a business analyst to handle it for them.
  In other words, you need to understand on an intricate level the math behind
 what you’re doing or you won’t be at the top.
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>



<!--Deck9-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--25-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-top" src="img/coding24.jpg" alt="image Can't load" height="200px" width="100%">
               <div class="card-body">
Y          <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
           <a data-toggle="modal" data-target="#exampleDeck-Y" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
      <div class="modal fade" id="exampleDeck-Y" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-Y" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-Y">Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
      <div class="modal-body">
      <blockquote>
      Landmark localization in driver monitoring adds a robust layer
      of safety to human driving, and can enable convenience features
      in upcoming autonomous vehicles. It can help determine if the
      cabin climate needs to be changed, or if a passenger will need
      a cupholder in the next few seconds. NVIDIA researchers presented
      their work on landmark localization and its variety of uses at the
      Conference of Computer Vision and Pattern Recognition in June,
      which you can read more about here
      </blockquote>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
      </div>
      </div>
      </div>
      </div>


    <!--26-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-top" src="img/coding25.jpg" alt="image Can't load" height="200px" width="100%">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
        <a data-toggle="modal" data-target="#exampleDeck-Z" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
   <div class="modal fade" id="exampleDeck-Z" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-Z" aria-hidden="true">
   <div class="modal-dialog" role="document">
   <div class="modal-content">
   <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-Z">Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
   </div>
   <div class="modal-body">
   <blockquote>
   However, gauging a driver’s attentiveness is no easy task.
    Their head is turned one way but their eyes could be looking
     in the opposite direction. Their eyes are open, but they could
      be exhausted and inattentive. That’s why NVIDIA’s deep learning
      algorithms use a method called landmark localization, identifying
       specific parts of an image to help infer the greater action that’s
        happening. For example, a driver may be facing forward with their
         eyes open. However, they’re blinking heavily while yawning and haven’t
         slowed yet for the traffic light that has just turned red. By reading
          the individual indicators on the driver’s face, the algorithms can
           determine they are drowsy and may not be paying attention,
            and step in to brake at the light.
   </blockquote>
   </div>
   <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
   </div>
   </div>
   </div>
   </div>
   </div>


<!--27-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-top" src="img/coding26.jpg" alt="image Can't load" height="200px" width="100%">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
   <a data-toggle="modal" data-target="#exampleDeck-AA" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-AA" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-AA" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-AA">Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
AI can help minimize these distractions and step in if necessary.
The NVIDIA DRIVE IX intelligent experience software stack allows car
 manufacturers to leverage AI as a new kind of copilot. Running on
  the NVIDIA DRIVE platform and using a driver-facing camera, the DRIVE
   IX deep learning algorithms can monitor drivers, detecting where their
   attention is focused and whether they are able to react to an oncoming
    situation. If the driver hasn’t noticed a critical obstacle, DRIVE IX
     can step in as a guardian angel, preventing them from taking
      an action that could be dangerous.
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<!--Deck10-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--28-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-top" src="img/coding27.jpg" alt="image Can't load" height="200px" width="100%">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
          <a data-toggle="modal" data-target="#exampleDeck-AB" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
      <div class="modal fade" id="exampleDeck-AB" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-AB" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-AB">Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
      <div class="modal-body">
      <blockquote>
       With a smartphone in hand or raucous kids in the back seat,
       human drivers are easily distracted, and these distractions
       can have dangerous consequences. If a driver takes their eyes
       off the road for just two seconds while traveling 65 miles per
       hour, they could travel about 200 feet without seeing what lies
       ahead or around them. In turn, these moments of distraction account
        for nearly 400,000 accidents in the U.S. each year,
       according to the National Highway Traffic Safety Administration
      </blockquote>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
      </div>
      </div>
      </div>
      </div>


    <!--29-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-top" src="img/coding28.jpg" alt="image Can't load" height="200px" width="100%">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
       <a data-toggle="modal" data-target="#exampleDeck-AC" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
   <div class="modal fade" id="exampleDeck-AC" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-AC" aria-hidden="true">
   <div class="modal-dialog" role="document">
   <div class="modal-content">
   <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-AC">Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
   </div>
   <div class="modal-body">
   <blockquote>
  Self-driving cars may not be in production yet, but driver assist technology in vehicles
  today continues to get more advanced, improving road safety. These technologies can alert
  a driver if a car is in their blind spot, keep vehicles centered in the lane or hit the
  brakes to avoid an oncoming collision. Some systems can even take over most driving functions
  on pre-mapped highways, controlling both steering and braking. While these features have enhanced
   the driving experience for many car owners, new advancements in driver assistance are using AI to focus on the driver themselves.
   </blockquote>
   </div>
   <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
   </div>
   </div>
   </div>
   </div>
   </div>


<!--30-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-top" src="img/coding29.jpg" alt="image Can't load" height="200px" width="100%">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
    <a data-toggle="modal" data-target="#exampleDeck-AD" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-AD" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-AD" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-AD">Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
  Five A Day is an amazingly useful database that gives you five web design and development related articles and tutorials every single day.
  CodeBette
  CodeBetter is a serious blog with fun illustrations about everything associated with coding, programming and web development.
  PHP Clicks
  PHP Clicks is a blog for advanced interactive tutorials. It covers HTML, CSS, jQuery, Angular JS, HTML5, CSS3,
   JavaScript, PHP, MySQL, WordPress, and much more.
  Tapiki blog
  Tapiki will help you learn more about Java coding and solving issues of debugging.
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!--Deck11-->
  <div class="container" style="padding-left:100px;">
    <hr>
    <!--31-->
      <div class="card-columns" style="margin-left:10px;">
         <div class="card" style="width:320px";>
           <div class="card-header" style="background:white !important;">
             <div class="row">
               <div class="col-md-2">
                 <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
               </div>
               <div class="col-md-5">
                 <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
                 <span style="font-size:10px;margin-left:4px !important">harison96</span>
               </div>
               <div class="col-md-5">
                 <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
               </div>
             </div>
           </div>
               <img class="card-img-top" src="img/coding30.jpg" alt="image Can't load" height="200px" width="100%">
               <div class="card-body">
            <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
                 <a data-toggle="modal" data-target="#exampleDeck-AE" class="text-primary">Read More</a>
        </div>
        <!-- Modal -->
      <div class="modal fade" id="exampleDeck-AE" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-AE" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-AE">Minimalist Baker</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
      <div class="modal-body">
      <blockquote>
        ontinued from page 1
      CodePen is the perfect place to share your latest coding creation with the crowd and get feedback on your work.Code Simplicity
      If you are looking for an expert who is writing about software development in a concrete and experienced way,
      check Max Kanat-Alexander work.Designs & Code
      Designs & Code is one of the best places for WordPress designers and developers.Line25
      Line25 is a blog publishing a combination of web development and design content.Livecoding.tv Blog
      Livecoding.tv is a programming blog packed with loads of video content to help you become a better developer.Antonio’s Blog
      Antonio is an expert developer blogging about Java development. He shares useful books, talks, training and articles.
      Java, SQL and JOOQ Java, SQL and JOOQ is a content rich blog full of tricks, tricks and best of the best coding practices.
      </blockquote>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
      </div>
      </div>
      </div>
      </div>

  <!--32-->
      <div class="card" style="width:320px";>
        <div class="card-header" style="background:white !important;">
          <div class="row">
            <div class="col-md-2">
              <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
            </div>
            <div class="col-md-5">
              <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
              <span style="font-size:10px;margin-left:4px !important">harison96</span>
            </div>
            <div class="col-md-5">
              <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
            </div>
          </div>
        </div>
            <img class="card-img-top" src="img/coding31.jpg" alt="image Can't load" height="200px" width="100%">
            <div class="card-body">
         <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
              <a data-toggle="modal" data-target="#exampleDeck-AF" class="text-primary">Read More</a>
     </div>
     <!-- Modal -->
   <div class="modal fade" id="exampleDeck-AF" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-AF" aria-hidden="true">
   <div class="modal-dialog" role="document">
   <div class="modal-content">
   <div class="modal-header">
   <h5 class="modal-title text-success text-center"  id="exampleDeckLabel-AF">Minimalist Baker</h5>
   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
   </div>
   <div class="modal-body">
   <blockquote>
     CSS-Tricks provides you 150 video screencasts about HTML, Flexbox, WordPress themes, building games with JavaScript and much more.
  Aphyr
  Aphyr blog is written by Kyle Kingsbury and mostly contains interesting insights about the coding world and code testing.
  Objc.io
  Objc.io is focused on iOS and OS X development. It provides a user-friendly and beautifully designed page with more than 20 issues on such topics as debugging, security, games, MVVM and more.
  David Walsh Blog
  DWB is a blog created by a web developer David Walsh and contributed by other coding experts. Browse the categories of various tutorials, features and demos.
   </blockquote>
   </div>
   <div class="modal-footer">
   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
   </div>
   </div>
   </div>
   </div>
   </div>


  <!--33-->
   <div class="card" style="width:320px";>
     <div class="card-header" style="background:white !important;">
       <div class="row">
         <div class="col-md-2">
           <img src="img/food.jpg" class="rounded-circle" height="40" width="40">
         </div>
         <div class="col-md-5">
           <b class="float-left" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important">Harison</b><br>
           <span style="font-size:10px;margin-left:4px !important">harison96</span>
         </div>
         <div class="col-md-5">
           <a href="#" style="line-height: 0.6;font-size:15px;padding-top:10px;padding-left:-5px !important" class="text-primary float-right">Follow</a>
         </div>
       </div>
     </div>
         <img class="card-img-top" src="img/coding32.jpg" alt="image Can't load" height="200px" width="100%">
         <div class="card-body">
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
           <a data-toggle="modal" data-target="#exampleDeck-AG" class="text-primary">Read More</a>
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleDeck-AG" tabindex="-1" role="dialog" aria-labelledby="exampleDeckLabel-AG" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title text-success text-center"  id="exampleDeckLabel-AG">Minimalist Baker</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<blockquote>
  The coding world might seem overwhelming at times - but when you
  pop, you can’t stop. So you might think you already know a lot,
   but there are always ways to sharpen your coding skills.
  In the days of fast development, especially in the technology
  field, it’s hard to keep up, and the theory books are no longer
   enough. So what can you do to become a better expert at what you
   do? The answer is simple - learn from the people who create and experiment.
  To help you do that, I have prepared a great list of 25 blogs to sharpen your
  coding skills. Go ahead and check it out. Scott Hanselman
  Scott Hanselman is a programmer, teacher and a speaker. He works for the Web Platform
   Team at Microsoft, and his blog was born from the passion he felt for his work.
  My Programming Blog
  My Programming Blog publishes tutorials and tips for ASP.NET, ActionScript, Github, C, Java,
</blockquote>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="container" style="padding-left:100px;">
  <hr>
</div>
<br>
<br>
<br>


<!--Pager-->
<div class="conatiner" style="margin-right:150px;margin-bottom:70px;">
  <ul class="breadcrumb bg-white justify-content-end">
    <li class="breadcrumb-item active">Prev</li>
    <li class="breadcrumb-item"><a href="#" class="success-link">1</a></li>
    <li class="breadcrumb-item"><a href="#" class="success-link">2</a></li>
    <li class="breadcrumb-item"><a href="#" class="success-link">3</a></li>
    <li class="breadcrumb-item"><a href="#" class="success-link">4</a></li>
    <li class="breadcrumb-item"><a href="#" class="success-link">Next</a></li>
  </ul>
</div>


<!--Foooter-->
<footer>
  <div class="jumbotron jumbotron-fluid  bg-dark" style="padding-top:-10px;">
     <!--footer links-->
     <div class="container text-center text-md-left">
        <!--Grid row-->
        <div class="row">
          <!--Grid Columns-->
          <div class="col-md-6 mx-auto text-white">
            <!--Content-->
            <h5 class="font-weight-bold text-uppercase mt-3 mb-4">About Devmins</h5>
            <p class="text-white">Devmins is simply awesome. With lots of features and elegant themes to choose from, it leads the blogging front.	</p>
          </div>
          <!--Grid columns-->
          <hr class="clearfix w-100 d-md-none">
          <div class="col-md-3 mx-auto">
            <!--Links-->
            <h5 class="font-weight-bol text-uppercase mt-3 mb-4 text-white">Useful links</h5>
            <ul class="list-unstyled text-white">
              <li>
                <a href="#" class="text-white">Your Account</a>
              </li>
              <li>
                <a href="#" class="text-white">Become Affiliate</a>
              </li>
              <li>
                <a href="#" class="text-white">Help</a>
              </li>
              <li>
                <a href="#" class="text-white">About Us</a>
              </li>
              <li>
                <a href="#" class="text-white">Know More</a>
              </li>
            </ul>
          </div>
          <!--Grid columns-->
          <hr class="clearfix w-100 d-md-none">
          <!-- Grid column -->
<div class="col-md-3 mx-auto">
  <h6 class="font-weight-bol text-uppercase mt-3 mb-4 text-white">Contact</h6>
    <ul class="list-unstyled text-white">
      <li><i class="fa fa-home mr-3"></i>G224, 4th Street ,Block G, Gamma-2, Greater Noida, U.P-201308</li>
      <li><i class="fa fa-envelope mr-3"></i> sarvesh@gmail.com</li>
      <li><i class="fa fa-phone mr-3"></i> + 91 96 547 914 68</li>
      <li><i class="fa fa-print mr-3"></i> + 01 99 115 859 87</li>
    </ul>
<!-- Grid column -->
        </div>
     </div>
     <hr>
  <!-- Social buttons -->
<ul class="list-inline text-center">
 <li class="list-inline-item">
   <a class="">
     <i class="fa fa-facebook"> </i>
   </a>
 </li>
 <li class="list-inline-item">
   <a class="">
     <i class="fa fa-twitter"> </i>
   </a>
 </li>
 <li class="list-inline-item">
   <a class="">
     <i class="fa fa-google"> </i>
   </a>
 </li>
 <li class="list-inline-item">
   <a class="">
     <i class="fa fa-linkedin"> </i>
   </a>
 </li>
 <li class="list-inline-item">
   <a class="">
     <i class="fa fa-instagram"> </i>
   </a>
 </li>
</ul>
<!-- Social buttons -->
<hr>

<!--Copyright-->
<div class="">
<div class="footer-copyright text-center text-muted" style="padding-bottom:50px;">@ 2018 Copyright:
 <a href="https://devmins.com" class="text-white">Devmins.com</a>
</div>
<!--Copyright-->
  </div>
</div>
 </footer>
</footer>
          </body>

        </html>
