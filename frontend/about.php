<?php
/**
 * Created by PhpStorm.
 * User: Nishant
 * Date: 02-06-2018
 * Time: 12:46
 */
include 'header.php';
include 'navbar.php';
?>


<!-- Page Header -->
<header class="masthead" style="background-image: url('img/ab4.jpg')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="page-heading">
                    <h1>Blogger</h1>
                    <span class="subheading">something! About blogging.</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">

            <p>Do you know what blogs are? If you don’t, then you’ve come to the right place. In the beginning, a blog was more of a personal diary that people shared online, and it goes back to 1994. On this online journal, you could talk about your daily life or share things you do. But, people saw an opportunity to communicate any information in a new way. So began the beautiful world of blogging.</p>
           <div class="containet">
               <h2>what is a blog?</h2>
               <hr>
               <h5>Definition of a blog</h5>>
            <p>A blog (shortening of “weblog”) is an online journal or informational website displaying information in the reverse chronological order, with latest posts appearing first. It is a platform where a writer or even a group of writers share their views on an individual subject.</p>
           </div>>

            <div >
                <h3>structure of blog</h3>
                <p>The appearance of blogs changed over time, and nowadays blogs include different items. But, most blogs include some standard features and structure. Here are common features that typical blog will include:</p>
                <hr>
                <ul>
                    <li>Header with the menu or navigation bar</li>
                    <li>Main content area with highlighted or latest blog posts</li>
                    <li>Sidebar with social profiles, favorite content, or call-to-action</li>
                    <li>Footer with relevant links like a disclaimer, privacy policy, contact page, etc.</li>
                </ul>>
            </div>
        </div>
    </div>
</div>

<hr>

<!-- Footer -->
<?php
include 'footer.php';
?>

